// import logo from './logo.svg';
import React , { Component, useState } from 'react';
import './App.css';
import Person from './Person/Person';
 const App = prop =>{

  const [personState,setPersonState] =useState({
    persons: [
      {name: 'Max', age: 28},
      {name: 'Manu', age: 23},
      {name: 'Jecinta', age: 22},
    ],
  });

 const [otherState,setOtherState] =useState("other other value");
 const  switchNameHandler = () => {
  setPersonState({
      persons: [
        {name: 'Maitrak', age: 28},
        {name: 'Manu', age: 23},
        {name: 'Jecinta', age: 21},
      ]
    })

  }


  return (

    <div className="App">
      <h1> Hello react go </h1>
      <p> It is working </p>
      <button onClick={switchNameHandler}> Click me</button>
      <Person name={personState.persons[0].name} age={personState.persons[0].age} />
      <Person name={personState.persons[1].name} age={personState.persons[1].age} >My Hobbies: Racing</Person>
      <Person name={personState.persons[2].name} age={personState.persons[2].age} />
    </div>

    // normal React.createElement Code

    // React.createElement('div',{className : 'App'},React.createElement('h1',null,"Hello"))
  );
  
}

export default App;
